#!/bin/python

# This script checks against for pwned passwords
# under https://haveibeenpwned.com/API/v3
# created by Troy Hunt

from random import randint
import getpass
import hashlib
import requests

# Functions

def sha1sum(password):
    m = hashlib.sha1()
    m.update(bytes(password, 'utf-8'))
    return m.hexdigest().upper()

def lookup(request, suffix):
    match=""
    url = "https://api.pwnedpasswords.com/range/{}".format(request)
    req = requests.get(url).content.decode('utf-8')
    for line in req.split('\n'):
        if suffix in line:
            match = line
    return match

def mypassword(password):
    amount = 0
    pwhash = sha1sum(password)
    request = pwhash[0:5]
    suffix = pwhash[6:]
    match = lookup(request, suffix)
    if match != "":
        amount = int(match[36:])
        print("The chosen password is " + str(amount) + " times in the pwned password lists")
    else:
        print("The chosen password is CURRENTLY not in the pwned password lists")
    return(amount)

print("")
print("--------------------")
print("Pwned Password Bingo")
print("--------------------")

# Needed Variables

nop = int(input("How many players: "))          #number of players (nop)
nor = int(input("How many round do we play: ")) #number or rounds (nor)
upperlimit = 0                                  #each round is a new upper limit
names = []
score = []
highscore = 0
winner = ""

# Initialize arrays

for i in range(0,nop):
    names.append(input("Name of Player " + str(i+1) + ": "))
for i in range(0,nop):
    score.append(0)

# Game-Loop

print("-")
for i in range(0,nor):
    print("-------")
    print("Round " + str(i+1))
    print("-------")
    for j in range (0,nop):
        print("-")
        upperlimit = randint(10000,25000)
        password = getpass.getpass("Player " + names[j] + ": Choose a password to check: ")
        myscore = mypassword(password)/1000
        if myscore <= upperlimit:
            print("You earn " + str(int(myscore)) + " score(s)!")
            score[j] = score[j] + int(myscore)
        else:
            print("You are over the random limit. Choose a not so obviously crappy password next time.")
        print("Current Points: " + str(score[j]))

# Specify the winner

print("-")
print("--------------------")
print("-- Award Ceremony --")
print("--------------------")
print("-")

for i in range(0,nop):
    print("Player " + str(i+1) + ": " + names[i] + " has " + str(score[i]) + " scores!")
    if highscore < score[i]:
        highscore = score[i]
        winner=names[i]
print("-")
print("The winner is " + winner + " with "  + str(highscore) + " scores!")
print("Congratiulations! You have chosen the worst passwords in this game!")
print("Don't get the bloody stupid idea of using them on a productive account.")