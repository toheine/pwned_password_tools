# Pwned Password Tools

## intro
Just a few tools to check for pwned passwords. See more information under [https://haveibeenpwned.com/](https://haveibeenpwned.com/)

## toc
* pwned-check.py &rarr; Simple request for a user-given password
* pwned-bingo.py &rarr; Little game to generate better awareness of people for bad passwords.
* lmn7-get-ntlms.sh &rarr; Copy this script on a LMN7-Server to gather NTLMs for a specific group
* lmn7-pwned-check.sh &rarr; Use this script to check gathered ntlms against pwned passwords

## pwned-check.py
To check if a password is in the data breaches use this tool. It is pretty safe, because only the first five characters of the local generated hash will sent to the API. The answer of the web query are several hundred suffixes that are available. The comparison then takes place locally.

## pwned-bingo.py
It is a simple game for one or more players. The objective is to think of a password that occurs as often as possible in the pwned-password files. The more often the password occurs, the more points the player receives. 

But be careful: in each game round, a maximum limit is set by random generator. As soon as the player exceeds this limit, he receives no points for his silly password. This keeps the game exciting and the player who chooses the password "123456" does not always win.

The Scripts inside uses the API from [https://haveibeenpwned.com/](https://haveibeenpwned.com/API/v3)

## lmn7-get-ntlms.sh
This script gather ntlms from a specific group on a LMN7-Server. You have to run it as root. If you call it without argument, you get a list of all available groups on your server. Take on of the given group as an agrument to gather your needed ntlms. E.g.:
```
root@srv:~/bin/# ./lmn7-get-ntlms.sh class10b
```
You can see the gathered ntlms on the terminal. Additionally a file is created according the pattern `<groupname>.ntlms`. You need this file for the next script.

## lmn7-pwned-check.sh
You need three things for this script:
* At least 30GB free disk space
* Last pwnd-passwords-ntlm-by-hash-vX.7z file from https://haveibeenpwned.com/Passwords
* Your gathered ntlms-file 

Copy both files in the same directory as this script.
Call the script like that:
```
user@anywhere:~/where/ever/you/want$ ./lmn7-pwned-check.sh <groupname>.ntlms
```
You get a feedback for each user on the terminal and in the file \<groupname\>.ntlms.checked. 
