#!/bin/bash

# use:
# sudo ./get-ntlm-hashes.sh <group>
# <group> is a specific class or teachers

### Settings

group=$1
dir=/srv/samba/schools/default-school

### Functions

function check_root() {
    if [ "$(whoami)" != root ]; then
        echo "Run this script as root."
        exit 1
    fi
}

function check_parameter() {
    echo "$group"
    if [ "$group" = "" ]; then
        echo "Take the group teachers or one of the following classes a parameter:"
        ls $dir/students/
        exit 0
    fi
}

function specify_dir() {
    if [ "$group" = "teachers" ]; then
	dir=$dir/teachers/
    else
	dir=$dir/students/$group/
	if [ ! -d "$dir" ]; then
	    echo "There is no group $group on this server!"
	    exit 2
	fi
    fi
}

function get_ntlms() {
    if [ -f "$group".ntlms ]; then
        rm "$group".ntlms
    fi

    for entry in "$dir"/*; do
	user=$(basename "$entry")
        echo -n "$user": | tee -a "$group".ntlms
        pdbedit -Lw "$user" | cut -d: -f4 | tee -a "$group".ntlms
    done

    chown -R "$SUDO_USER":"$SUDO_USER" "$group".ntlms
    chmod 600 "$group".ntlms
}

### Script Start

check_root
check_parameter
specify_dir
get_ntlms
