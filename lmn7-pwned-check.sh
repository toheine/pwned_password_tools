#!/bin/bash

# use:
# Download pwnd-passwords-ntlm-by-hash-v7.7z file from
# https://haveibeenpwned.com/Passwords
# to the same directory as this script
# Call the script like that:
# ./lmn7-pwned-check.sh <group>.ntlms
#<group> is a specific class or teachers

## Settings

#Pwend-Password-File
ppf=pwned-passwords-ntlm-ordered-by-hash-v8

#Gathered NTLMs to check
#File must have the following structure
#<username>:<ntlm-hash>
#file can be created by using get-ntlm-hashes.sh on a lmn7-server
userfile=$1

#Outputfile
outfile=$userfile.checked

## Functions

function check_env(){

    # Check if 7zip is installed
    if [ -z "$(command -v 7z)" ]; then
        echo "p7zip is required, but not installed!"
        exit 1
    fi

    # Check if pwned-password-file is available
    if [ -f $ppf.txt ]; then
        echo "Pwned Password-File exists"
    else
        if [ -f $ppf.7z ]; then
            echo "Pwned-Password-File exists compressed"
            $(command -v 7z) e $ppf.7z
        else
            echo "The required pwned-passwords-ntlm file is not present!"
            echo "Download $ppf.7z from:"
            echo "https://haveibeenpwned.com/Passwords"
            exit 2
        fi
    fi
}

function check_pwned(){

    # Delete outfile of an older check if exists
    if [ -f "$outfile" ];then
        rm "$outfile"
    fi

    # Check line by line for pwned passwords
    for line in $(cat "$userfile"); do
        username=$(echo "$line" | cut -d: -f1)
        userhash=$(echo "$line" | cut -d: -f2)
        echo -n "$line": | tee -a "$outfile"
        match=$(grep "$userhash" $ppf.txt)
        if [ -z "$match" ]; then
            echo "---" | tee -a "$outfile"
        else
            count=$(echo "$match" | cut  -d: -f2)
            echo "$count" | tee -a "$outfile"
        fi
    done
}

check_env
check_pwned
