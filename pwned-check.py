#!/bin/python

import getpass
import hashlib
import requests

# Functions

def sha1sum(password):
    m = hashlib.sha1()
    m.update(bytes(password, 'utf-8'))
    return m.hexdigest().upper()

def lookup(request, suffix):
    match=""
    url = "https://api.pwnedpasswords.com/range/{}".format(request)
    req = requests.get(url).content.decode('utf-8')
    for line in req.split('\n'):
        if suffix in line:
            match = line
    return match

# Let's start

print("")
print("-----------------------------")
print("Simple Pwned Password Checker")
print("-----------------------------")

password = getpass.getpass("Choose a password to check: ")
pwhash = sha1sum(password)
request = pwhash[0:5]
suffix = pwhash[6:]
match = lookup(request, suffix)

if match != "":
    print("Match found: " + match)
    amount = int(match[36:])
    print("The chosen password is " + str(amount) + " times in the pwned password lists")
else:
    print("The chosen password is CURRENTLY not in the pwned password lists")

print("")

